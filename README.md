kde:neon-kde-org

# Requirements

- apache
- php(7)
- htaccess + mod_rewrite need to be enabled
  - sudo a2enmod rewrite
  - edit /etc/apache2/sites-enabled

```
<Directory /var/www/>
     Options Indexes FollowSymLinks
     AllowOverride All
     Require all granted
</Directory>
```

# Font Glyphs

Font glyphs are managed via icomoon.io.
To edit import the svg and add new svg glyphs to the set then export as Font (see bottom bar).

Relevant export settings (rest default):
- Name: glyph
- Class Prefix: glyph
- FontMetrics
  - Em Square Height: 512

After export copy the fonts directory into our glyph directory and update css/glyph.css (there's a style.css in the export from icomoon you can pick the new rules from)
