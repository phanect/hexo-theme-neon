<?php
/**
 * Available variables for the header:
 * $headerCssFiles = [];
 * $headerJsFiles = [];
 * $headerBodyClasses = '';
 */
	$headerCssFiles = ['content/download.css'];
	$headerBodyClasses = '';
	include('templates/header.php');
?>


	<main class="KLayout">
		<section>
			<article>
			<h1 id="live">Live Images</h1>
			<p>
				Live images are ideal for people who want a clean installation. Use a
				live image to replace your existing system, install alongside
				existing operating systems, or simply try KDE neon without affecting their
				computer.
			</p>
			</article>
			<article class="multicolumn-2">
				<div>
					<img src="content/download/userMedia.svg" class="splashImage" />
					<h3>User Edition<sup class="tag">64-bit</sup></h3>
					<p style="text-align: center;">
						Featuring the latest officially released KDE software on a stable
						base. Ideal for everyday users.
					</p>
					<div class="downloadButton">
						<a href="http://files.kde.org/neon/images/neon-useredition/current/">User Edition Live/Install Images</a>
					</div>
					<h3>User LTS Edition<sup class="tag">64-bit</sup></h3>
					<p style="text-align: center;">
						Featuring the latest officially released KDE software on a stable Plasma 5.8 LTS desktop and a stable
						base. Ideal for everyday users who want new applications but a stable desktop.
					</p>
					<div class="downloadButton">
						<a href="http://files.kde.org/neon/images/neon-userltsedition/current/">User LTS Edition Live/Install Images</a>
					</div>
                </div>
				<div>
					<img src="content/download/developerMedia.svg" class="splashImage" />
					<h3>Developer Edition Git-Unstable<sup class="tag">64-bit</sup></h3>
					<p style="text-align: center;">
						Featuring pre-release KDE software built
						the same day from new feature branches.
					</p>
					<div class="downloadButton">
						<a href="http://files.kde.org/neon/images/neon-devedition-gitunstable/current/">Developer Edition Git-Unstable Branch Images</a>
					</div>
					<h3>Developer Edition Git-Stable<sup class="tag">64-bit</sup></h3>
					<p style="text-align: center;">
						Featuring pre-release KDE software built
						the same day from bugfix branches.
					</p>
					<div class="downloadButton">
						<a href="http://files.kde.org/neon/images/neon-devedition-gitstable/current/">Developer Edition Git-Stable Branch Images</a>
					</div>
				</div>
				
			</article>
				<div style="text-align: center;">
					<a href="http://srcfiles.neon.kde.org/" target="_blank" style="color: #909c9c;">Download Source ISOs</a>
				</div>
			<article>
					<p class="">
						Install using ROSA Image Writer for 					</p>

<ul><li> <a href="http://wiki.rosalab.ru/en/images/6/65/RosaImageWriter-2.6.1-win.zip" class="internal" title="RosaImageWriter-2.6.1-win.zip">Windows</a>
						</li><li> <a href="http://wiki.rosalab.ru/en/images/b/bd/RosaImageWriter-2.6.1-lin-i686.txz" class="internal" title="RosaImageWriter-2.6.1-lin-i686.txz">Linux 32-bit</a>
						</li><li> <a href="http://wiki.rosalab.ru/en/images/2/24/RosaImageWriter-2.6.1-lin-x86_64.txz" class="internal" title="RosaImageWriter-2.6.1-lin-x86 64.txz">Linux 64-bit</a>
						</li><li> <a href="http://wiki.rosalab.ru/en/images/e/ea/RosaImageWriter-2.6.1-osx.dmg" class="internal" title="RosaImageWriter-2.6.1-osx.dmg">Mac OS X</a>
						</li></ul>

					<p class="">GPG signatures
					signed by <a
					href="https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xDEACEA00075E1D76">KDE
					neon ISO Signing Key</a> (0xDEACEA00075E1D76) are
					available alongside the ISOs
					for verification.</p>

					<p>Note: we add a machine ID to update checks for install counting, <a href="faq.php#installCounting">FAQ</a>.</p>
			</article>
			<article>
			<h1 id="live">Docker Images</h1>
			<p>
                <a href="https://community.kde.org/Neon/Docker"><img src="images/moby.svg" class="splashImage" width="400" /></a>
				Docker Images are great for testing or developing on KDE software without the need to re-install your operating system or a virtual machine.  We build ours daily on Docker Hub.
				<div class="downloadButton">
						<a href="https://community.kde.org/Neon/Docker">Docker Images Documentation</a>
				</div>
			</p>
			</article>
		</section>
	</main>

<?php

	include ('templates/footer.php');
?>
