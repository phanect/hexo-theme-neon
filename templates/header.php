<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>KDE neon</title>

	<!-- IE up to 9, excluding 10, ignoring this rubbish. -->
	<!--[if IE]><link href="media/favicon.ico" rel="shortcut icon" /><![endif]-->
	<!-- IE11/Chrome/Opera/Safari -->
	<link href="media/192x192.png" rel="icon" sizes="192x192" />
	<!-- iOS -->
	<link href="media/180x180.png" rel="apple-touch-icon" sizes="180x180" />
	<!-- Sane Browsers -->
	<link href="media/128x128.svg" rel="icon" sizes="128x128" />
	<link href="media/64x64.svg" rel="icon" sizes="64x64" />
	<link href="media/32x32.svg" rel="icon" sizes="32x32" />
	<link href="media/24x24.svg" rel="icon" sizes="24x24" />
	<link href="media/16x16.svg" rel="icon" sizes="16x16" />

	<!-- JQuery -->
	<script src="media/jquery-2.1.4.min.js"></script>

	<!-- KGlobalHeader files -->
	<script src="cdn/kGlobalAssets.js?include=header,footer"></script>
	<link href="cdn/kGlobalAssets.css?include=header,subHeader,footer,layout,social" rel="stylesheet" type="text/css" />

	<link href="page.css" rel="stylesheet" type="text/css" />
	<?php

	if (isset($headerCssFiles))
		foreach($headerCssFiles as $file)
			echo "\n\t".'<link href="'.$file.'" rel="stylesheet" type="text/css" />';

	?>

	<?php

	if (isset($headerScriptFiles))
		foreach($headerScriptFiles as $file)
			echo "\n\t".'<script src="'.$file.'"></script>';

	?>
</head>
<body class="KStandardSiteStyle" style="min-width: 1000px;">

	<header id="KGlobalHeader">
		<nav id="KGlobalNavigation">
			<h1 id="KGlobalLogo"><a href="https://kde.org">KDE</a></h1>
			<ul id="KGlobalStaticNavigation">
				<li class="community">Community
					<section>
						<nav>
							<a href="irc://irc.freenode.net/%23kde-neon">#kde-neon on Freenode IRC</a>
							<a href="https://mail.kde.org/mailman/listinfo/neon">KDE neon mailing list</a>
							<a href="https://forum.kde.org/viewforum.php?f=309">KDE Forum</a>
							<a href="https://telegram.me/kdeneon">Telegram KDE neon users group</a>
							<a href="https://www.facebook.com/groups/931803210238672/">Facebook KDE neon users group</a>
							<a href="https://plus.google.com/communities/105101838887387505413">G+ KDE neon community</a>
						</nav>
					</section>
				</li><li class="products">Products
				</li><li class="develop">Develop
				</li><li class="donate">Donate
				</li><li class="support">Support
				</li>
			</ul>
			<ul id="KGlobalDynamicNavigation"></ul>
		</nav>
		<section id="KSiteNavigation" class="<?php if(defined('OVERLAY')) echo 'overlay'; ?>">
			<div>
				<h1><a href="index"><img src="content/neon-logo.svg" />KDE neon</a></h1>
				<nav>
					<a href="faq">FAQ</a>
					<a href="develop">Developers' Edition</a>
					<a href="slimbook" class="button">KDE Slimbook</a>
					<a href="download" class="button">Download KDE neon</a>
				</nav>
			</div>
		</section>
	</header>
