<?php
	include('templates/header.php');
?>


	<main class="KLayout">
		<section>
			<article id="what-is-neon">
				<h1 class="faqQuestion">FAQ</h1>
				<h4 class="faqQuestion">What is KDE neon?</h4>
				<div>
					<p>

						KDE neon is a rapidly updated software
						repository.  Most users will want to use the packages built from released software which will be available soon.  KDE contributors and testers can use the packages built from KDE Git.  It uses the foundation of the latest Ubuntu LTS (16.04).

					</p>
				</div>
			</article>

			<article id="is-this-the-distro">
				<h4 class="faqQuestion">Is this &quot;the KDE distro&quot;?</h4>
				<div>
					<p>
						Nope. KDE believes it is important to work with many
						distributions, as each brings unique value and
						expertise for their respective users.  This is one project out of hundreds from KDE.
					</p>
				</div>
			</article>

			<article id="non-lts-releases">
				<h4 class="faqQuestion">Will there be a version using non LTS Ubuntu releases?</h4>
				<div>
					<p>No, we plan only to base on
					the latest LTS version of
					Ubuntu, this comes with Linux
					and graphics stack updates to
					keep drivers relevant.  We
					will backport other software
					as needed.</p>
				</div>
			</article>

			<article id="is-it-a-distro">
				<h4 class="faqQuestion">Is it a distro?</h4>
				<div>
					<p>
						Not quite, it's a
						package archive with
						the latest KDE
						software on top of a
						stable base. While we
						have
						installable images,
						unlike full Linux distributions
						we're only interested
						in KDE software.
					</p>
				</div>
			</article>

			<article id="is-it-rolling">
				<h4 class="faqQuestion">Is it a rolling distro?</h4>
				<div>
					<p>
						Not quite, it's a
						package archive with
						the latest KDE
						software on top of a
						stable base.  Unlike
						rolling distros only
						the KDE software will
						be updated
						continuously.
					</p>
				</div>
			</article>

			<article id="diff-ubuntu">
				<h4 class="faqQuestion">What is the difference between KDE neon and using plain Ubuntu?</h4>
				<div>
					<p>
						KDE neon will provide users with more up-to-date
						packages of Qt and cutting-edge KDE software.
					</p>
				</div>
			</article>

			<article id="why-ubuntu">
				<h4 class="faqQuestion">Why Ubuntu?</h4>
				<div>
					<p>
						We use Ubuntu as a base for KDE neon because the KDE neon team feels it offers
						the best technology as a stable release and the
						best third party support. The KDE neon team is familiar
						with Ubuntu having worked with it for over a decade.
						We also feel that Ubuntu users will miss out if they
						do not have up to date KDE software.  It is otherwise unrelated to the
						Ubuntu project or Canonical.
					</p>
				</div>
			</article>

			<article id="morph-kubuntu">
				<h4 class="faqQuestion">Can I turn Kubuntu into KDE neon with a PPA?</h4>
				<div>
					<p>
						We recommend that you install a fresh KDE neon from the provided
						ISO images.
						But you can indeed add an APT repository to switch from Kubuntu
						to KDE neon. This is absolutely not tested or supported. If things
						take a turn for the worse you are expected to be knowledgable enough
						to repair your system on your own. A web search should quickly give
						you relevant information on how to do this.
					</p>
				</div>
			</article>

			<article id="diff-kubuntu">
				<h4 class="faqQuestion">Is KDE neon an add-on to Kubuntu?</h4>
				<div>
					<p>
						KDE neon sits on top of the Ubuntu core foundations, which means
						the majority of software built for Ubuntu core will work fine,
						even when not	explicitly supported by the KDE neon team. KDE neon is
						however not	compatible with Kubuntu, as there is vast overlap in the
						base offerings of both Kubuntu and KDE neon. You can not use both
						systems at the same time. Installing KDE neon will simply replace
						Kubuntu.
					</p>
				</div>
			</article>

			<article id="diff-desktop">
				<h4 class="faqQuestion">Can I use a desktop other than Plasma?</h4>
				<div>
					<p>
						This probably won't work (probably won't even install). It
						most certainly isn't supported even if the desktop installs. KDE
						neon focuses on KDE software, most other software is not supported
						and you should not be surprised if you can not install it or it
						stops working at any point in time due to an update.
					</p>
				</div>
			</article>

			<article id="64bit">
				<h4 class="faqQuestion">Is KDE neon 64bit only?</h4>
				<div>
					<p>
						Yes. As computers without 64bit have become increasingly rare,
						we have chosen to focus our resources on higher-quality 64bit builds.
					</p>
				</div>
			</article>

			<article id="ssl-mirrors">
				<h4 class="faqQuestion">Why are the download mirrors not https?</h4>
				<div>
					<p>

                                                The mirrors are generously donated for KDE use by various organisations
						but several don't support https so our downloads are http only.  However
						the images are GPG signed so you can be sure they match what was created by our builders.
						
					</p>
				</div>
			</article>

			<article id="installCounting">
				<h4 class="faqQuestion">How many installs of KDE neon are there?</h4>
				<div>
					<p>We include the machine ID in version update requests in
					/etc/update-manager/meta-release and hope to have a website up shortly which shows figures on install numbers.</p>

					<p>To remove install counting run </p>
					<p><code>sed s,meta-release.*,meta-release, /etc/update-manager/meta-release</code>.</p>
					This will be added as an option in the installer when we get a moment.
					</p>
				</div>
			</article>

			<article id="what-is-kde">
				<h4 class="faqQuestion">What is KDE?</h4>
				<div>
					<p>
						The KDE® Community is an international technology team dedicated
						to creating a free and user-friendly computing experience, offering
						an advanced graphical desktop, a wide variety of applications for
						communication, work, education and entertainment and a platform to
						easily build new applications upon.
					</p>
				</div>
			</article>
		</section>
	</main>

<?php

	include ('templates/footer.php');
