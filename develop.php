<?php
	include('templates/header.php');
?>


	<main class="KLayout">

		<section>
				<article>
					<h1>Home Grown for Development</h1>
					<p>
						While KDE neon is great for casual users,
						contributors will find KDE neon excels.
						Whether you are a coder, tester, artist, or do
						bug triage: KDE neon is the easiest way to work
						on the latest KDE software.
					</p>
					<p>Our developers' archive provides packages of KDE Software built on our continuous integration system directly from Git.</p>
					<p>
						We believe open software developed outside a single
						corporate silo is important for healthy projects to
						incubate and grow.
						Ubuntu has been chosen as a base for its reliable core
						technology, but KDE neon itself is a separate KDE
						project aiming to best serve the interest of
						developers, users and the wider community.
					</p>
				</article>
		</section>

<div id="devApps" style="height: 500px; width: 100%; background-color: #34495e; background-image: url(content/develop/devApps.png); background-position: center; background-size: auto 100%; background-repeat: no-repeat;"></div>

		<section>
				<article>
					<h1>The Tools you Need</h1>
					<p>
						You are what you make and the applications from
						the KDE community are there to help you create. No
						matter how complex your project, they will be there.
						From text editors and development tools to art
						applications and video editors, all at your
						fingertips, all at your disposal and on your own terms.
					</p>
					<p>
						Everyone in the KDE community has access to fix or update our packaging.  Take a look at the <a href="http://packaging.neon.kde.org">KDE neon Git repositories</a> to browse the packaging (<a href="https://community.kde.org/Neon/Git">instructions for use on wiki</a>) or to see the status of the builds browse the <a href="http://build.neon.kde.org">KDE neon Build server</a>.
					</p>
					<p>
						More information is on the <a href="https://community.kde.org/Neon">KDE neon wiki pages</a> to find out who’s making this and how to help us.  Developers mostly chat in <a href="irc://irc.freenode.net/%23kde-neon">#kde-neon on Freenode IRC</a> and on the <a href="https://mail.kde.org/mailman/listinfo/neon">KDE neon mailing list</a>.
					</p>
					<p>
					Found a beastie?  We are part of KDE so just report it on <a href="http://bugs.kde.org">bugs.kde.org</a>.  If the bug is part of the application report it there.  If the bug is part of the packaging then <a href="https://bugs.kde.org/describecomponents.cgi?product=neon">report it on neon</a>.  Currently the installer is Ubiquity from Ubuntu so you can try <a href="https://launchpad.net/ubuntu/+source/ubiquity">reporting bugs on Launchpad</a>.
					</p>
				</article>
		</section>
	</main>

<?php

	include ('templates/footer.php');
?>
